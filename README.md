# Moklet Goods Remainder

## Link Google Play
https://drive.google.com/file/d/12UQjEXBy8TCW7IIyLdQsWgLq069I_KmQ/view?usp=sharing

## Identitas
  > Farhan Irsyadil Aqshala (XI RPL 3 / 14)

  > Farodhilah Nur Kholifatur  Ridho (XI RPL 3 / 15)

  > Salsabilla Prillia Fandi (XI RPL 3 / 34)

  > Nama Sekolah : SMK Telkom Malang

  > Angkatan : 25

## Deskripsi
  > Aplikasi ini dibuat untuk menginformasikan kepada warga SMK Telkom Malang tentang barang-barang yang tertinggal. Sehingga warga SMK Telkom Malang tidak perlu menuju ke sarpra untuk melihat daftar barang-barang yang tertinggal.

## Fungsi Aplikasi
  > Melihat list barang tertinggal di area SMK TELKOM MALANG
  
  > Mengajukan pengambilan untuk barang tertinggal
  
  > Upload bukti form

## Cara Menggunakan Aplikasi
  > Login
  
  > Lihat list dan temukan barang anda
  
  > Klik button "ambil"
  
  > Klik ikon tambah untuk memilih gambar (foto bukti form)
  
  > Klik "upload" untuk mengupload foto bukti form
  
  > Temui dan ambil barang anda di Sarpra

## Screenshoot
  ![Screenshot_2018-05-01-05-04-10-393_id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder](/uploads/06bffcc417e150263e438d2408d8da2f/Screenshot_2018-05-01-05-04-10-393_id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder.png)
  ![Screenshot_2018-05-01-05-04-40-648_id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder](/uploads/b58df04a97a81879de6289d963331af9/Screenshot_2018-05-01-05-04-40-648_id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder.png)
  ![Screenshot_2018-05-01-05-04-22-640_id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder](/uploads/a8d5f0c44f2603b79d77b76063b25144/Screenshot_2018-05-01-05-04-22-640_id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder.png)
  ![Screenshot_2018-05-01-05-06-19-269_id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder](/uploads/7fff16b791df22c4d9880d1bd36cad6a/Screenshot_2018-05-01-05-06-19-269_id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder.png)
  ![Screenshot_2018-05-01-05-07-28-234_id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder](/uploads/0fbc38d2c1585fb89b23368308bcadba/Screenshot_2018-05-01-05-07-28-234_id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder.png)
  ![Screenshot_2018-05-01-05-06-27-675_id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder](/uploads/dbc5f593f769ee2dba681f3d14010436/Screenshot_2018-05-01-05-06-27-675_id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder.png)
  ![Screenshot_2018-05-01-05-02-51-284_id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder](/uploads/d33f1d7ee8c68124144fd03cedcc43b0/Screenshot_2018-05-01-05-02-51-284_id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder.png)
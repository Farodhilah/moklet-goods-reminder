package id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class BarangListAdapter extends ArrayAdapter<Barang> {

    FirebaseAuth mAuth;
    private Activity context;
    private int resource;
    private List<Barang> listBarang;

    public BarangListAdapter(Activity context, int resource, List<Barang> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        listBarang = objects;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        mAuth = FirebaseAuth.getInstance();
        LayoutInflater layoutInflater = context.getLayoutInflater();
        View v = layoutInflater.inflate(resource, null);
        TextView textJudul = v.findViewById(R.id.textJudul);
        TextView textTempat = v.findViewById(R.id.textTempat);
        TextView textTanggal = v.findViewById(R.id.textTanggal);
        ImageView imgBarang = v.findViewById(R.id.imgBarang);
        final CardView cardView = v.findViewById(R.id.cardView);
        final ImageView imgNotif = v.findViewById(R.id.imgNotif);
        final Button btnAmbil = v.findViewById(R.id.btnAmbil);

        textJudul.setText(listBarang.get(position).getNama());
        textTempat.setText(listBarang.get(position).getTempat());
        textTanggal.setText(listBarang.get(position).getTanggal());
        Glide.with(context).load(listBarang.get(position).getUrl()).into(imgBarang);


        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("admin").child("email");
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                String role = mAuth.getCurrentUser().getEmail();
//                Toast.makeText(getApplicationContext(),role,Toast.LENGTH_LONG).show();
                if (value.equals(role)) {
                    btnAmbil.setVisibility(View.INVISIBLE);
                    if (listBarang.get(position).getNotif().equals("0")) {
                        imgNotif.setVisibility(View.INVISIBLE);
                    }
                    cardView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, LihatActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("ID", listBarang.get(position).getId());
                            intent.putExtra("url", listBarang.get(position).getUrl());
                            intent.putExtra("url2", listBarang.get(position).getAmbil());
                            intent.putExtra("tanggal", listBarang.get(position).getTanggal());
                            intent.putExtra("nama", listBarang.get(position).getNama());
                            intent.putExtra("tempat", listBarang.get(position).getTempat());
                            intent.putExtra("notif", listBarang.get(position).getNotif());
                            context.startActivity(intent);
                            context.finish();
                        }
                    });
                } else {
                    imgNotif.setVisibility(View.INVISIBLE);
                    btnAmbil.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, AmbilActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("ID", listBarang.get(position).getId());
                            intent.putExtra("url", listBarang.get(position).getUrl());
                            intent.putExtra("tanggal", listBarang.get(position).getTanggal());
                            intent.putExtra("nama", listBarang.get(position).getNama());
                            intent.putExtra("tempat", listBarang.get(position).getTempat());
                            context.startActivity(intent);
                            context.finish();
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("", "Failed to read value.", error.toException());
            }
        });


        return v;

    }
}

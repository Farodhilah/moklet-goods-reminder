package id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder;

public class Barang {
    public String id;
    public String notif;
    public String ambil;
    public String url;
    public String tanggal;
    public String nama;
    public String tempat;

    public Barang() {
    }

    public Barang(String id, String url, String tanggal, String nama, String tempat, String notif, String ambil) {
        this.id = id;
        this.url = url;
        this.tanggal = tanggal;
        this.nama = nama;
        this.tempat = tempat;
        this.notif = notif;
        this.ambil = ambil;
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getTanggal() {
        return tanggal;
    }

    public String getNama() {
        return nama;
    }

    public String getTempat() {
        return tempat;
    }

    public String getNotif() {
        return notif;
    }

    public String getAmbil() {
        return ambil;
    }


}

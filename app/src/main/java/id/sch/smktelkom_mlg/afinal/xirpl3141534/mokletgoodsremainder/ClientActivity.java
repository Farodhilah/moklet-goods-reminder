package id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ClientActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    DatabaseReference databaseReference;
    List<Barang> brglist;
    ListView lv;
    BarangListAdapter adapter;
    ProgressDialog progressDialog;
    FirebaseAuth mAuth;
    Context context = this;
    String choice;
    SharedPreferences pref;
    GoogleSignInClient mGoogleSignInClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_klien);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAuth = FirebaseAuth.getInstance();
        pref = getSharedPreferences("myPrefer", Context.MODE_PRIVATE);

        brglist = new ArrayList<>();
        lv = findViewById(R.id.listViewBarang);

        //show progress dialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Tunggu Sebentar...");
        progressDialog.show();

        databaseReference = FirebaseDatabase.getInstance().getReference(TambahActivity.FB_Database_Path);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressDialog.dismiss();
                //fetch image data from firebase
                for (DataSnapshot Snapshot : dataSnapshot.getChildren()) {
                    Barang brg = Snapshot.getValue(Barang.class);
                    brglist.add(brg);
                }

                //init adapter
                adapter = new BarangListAdapter(ClientActivity.this, R.layout.barang_item, brglist);
                lv.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        LinearLayout mParent = (LinearLayout) navigationView.getHeaderView(0);
        String url = pref.getString("img_url", null);
        String nama_akun = pref.getString("nama", null);
        String email_akun = pref.getString("email", null);
        ImageView foto = mParent.findViewById(R.id.foto2);
        TextView nama = mParent.findViewById(R.id.nama2);
        TextView email = mParent.findViewById(R.id.email2);
        Glide.with(this).load(url).into(foto);
        nama.setText(nama_akun);
        email.setText(email_akun);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.logout2) {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(ClientActivity.this, LoginActivity.class)); //Go back to home page
            finish();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

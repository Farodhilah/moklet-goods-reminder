package id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileNotFoundException;
import java.io.IOException;

public class AmbilActivity extends AppCompatActivity {


    public static final String FB_Storage_Path = "image/";
    public static final String FB_Database_Path = "barang";
    public static int Request_code = 1234;
    private StorageReference storageReference;
    private DatabaseReference databaseReference;
    private ImageView imageView;
    private Button btnUpload;
    private Uri imguri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambil);

        FirebaseStorage storage = FirebaseStorage.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference(FB_Database_Path);

        storageReference = storage.getReference();
        imageView = findViewById(R.id.imageView2);
        btnUpload = findViewById(R.id.button2);

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnUpload();
            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnBrowse_Click();
            }
        });


    }

    public void btnBrowse_Click() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "SelectImage"), Request_code);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Request_code && resultCode == RESULT_OK && data != null && data.getData() != null) {
            imguri = data.getData();
            try {
                Bitmap bm = MediaStore.Images.Media.getBitmap(getContentResolver(), imguri);
                imageView.setImageBitmap(bm);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getImageExt(Uri uri) {

        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();

        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    @SuppressWarnings("VisibleForTest")
    public void btnUpload() {
        if (imguri != null) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setTitle("Uploading Image...");
            dialog.show();

            //get the storage ref
            StorageReference ref = storageReference.child((FB_Storage_Path + System.currentTimeMillis() + "." + getImageExt(imguri)));

            //add file to ref
            ref.putFile(imguri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    dialog.dismiss();
                    //display success Toast
                    Toast.makeText(getApplicationContext(), "Image Upload", Toast.LENGTH_SHORT).show();
                    String id = getIntent().getStringExtra("ID");
                    String url = getIntent().getStringExtra("url");
                    String tanggal = getIntent().getStringExtra("tanggal");
                    String nama = getIntent().getStringExtra("nama");
                    String tempat = getIntent().getStringExtra("tempat");
                    Barang imageUpload = new Barang(id, url, tanggal, nama, tempat, "1", taskSnapshot.getDownloadUrl().toString());

                    //save image info
                    databaseReference.child(id).setValue(imageUpload);
                    startActivity(new Intent(getApplicationContext(), ClientActivity.class));
                    finish();

                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            //dismiss dialog
                            dialog.dismiss();
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            dialog.setMessage("Uploading..." + (int) progress + "%");
                        }
                    });

        } else {
            Toast.makeText(getApplicationContext(), "Please Select Image", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), ClientActivity.class));
        finish();
    }
}

package id.sch.smktelkom_mlg.afinal.xirpl3141534.mokletgoodsremainder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LihatActivity extends AppCompatActivity {

    ImageView imageLihat;
    TextView textView3, textView4, textView5;
    Button btnKonfirmasi;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat);

        imageLihat = findViewById(R.id.imageLihat);
        textView3 = findViewById(R.id.textView3);
        textView4 = findViewById(R.id.textView4);
        textView5 = findViewById(R.id.textView5);
        btnKonfirmasi = findViewById(R.id.btnKonfirmasi);

        final String id = getIntent().getStringExtra("ID");
        String url = getIntent().getStringExtra("url");
        final String url2 = getIntent().getStringExtra("url2");
        final String tanggal = getIntent().getStringExtra("tanggal");
        final String nama = getIntent().getStringExtra("nama");
        final String tempat = getIntent().getStringExtra("tempat");

        textView3.setText(nama);
        textView4.setText(tempat);
        textView5.setText(tanggal);
        if (url2.equals("")) {
            Glide.with(getApplicationContext()).load("http://cdn.iseated.com/assets/img/nopicture.jpg").into(imageLihat);
        } else {
            Glide.with(getApplicationContext()).load(url2).into(imageLihat);
        }

        btnKonfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                databaseReference = database.getReference("barang");

                //save image info
                databaseReference.child(id).removeValue();
                startActivity(new Intent(getApplicationContext(), AdminActivity.class));
                finish();
            }
        });


    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), AdminActivity.class));
        finish();
    }
}
